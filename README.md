## Stored Procedure :

----- STORED PROCEDURE QUERY #1 -----
DELIMITER $
CREATE PROCEDURE findAllEmployees ()
	BEGIN
		SELECT * FROM employee;
	END $
DELIMITER ;


----- STORED PROCEDURE QUERY #2 -----
DELIMITER $
CREATE PROCEDURE findEmployeeByDepartment (IN emp_department VARCHAR(200))
	BEGIN
		SELECT * FROM employee emp WHERE emp.edept = emp_department;
	END $
DELIMITER ;