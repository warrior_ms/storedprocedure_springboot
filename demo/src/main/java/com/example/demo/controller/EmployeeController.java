package com.example.demo.controller;

import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController

public class EmployeeController {

    @Autowired
    private EmployeeDao empdao;

    @GetMapping("/getallemp")
    public Iterable<Employee> getAllEmp()
    {
        return empdao.getAllEmployees();
    }

    @GetMapping("/getempbydept/{employee-department}")
    public Iterable<Employee> getEmployeesByDept(@PathVariable(name = "employee-department") String dept)
    {
        return empdao.getEmployeeByDepartment(dept);
    }
}
