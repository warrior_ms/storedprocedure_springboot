package com.example.demo.repository;

import com.example.demo.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class EmployeeDao {

    @Autowired
    private EntityManager em;

    @SuppressWarnings("unchecked")
    public Iterable<Employee> getAllEmployees()
    {
        return em.createNamedStoredProcedureQuery("procedure-one").getResultList();
    }

    @SuppressWarnings("unchecked")
    public Iterable<Employee> getEmployeeByDepartment(String input)
    {
        return em.createNamedStoredProcedureQuery("procedure-two").setParameter( "emp_department", input).getResultList();
    }

    @SuppressWarnings("unchecked")
    public Integer getEmployeesCountByDesignation(String input)
    {
        return (Integer) em.createNamedStoredProcedureQuery("procedure-three").setParameter("emp_designation", input).getOutputParameterValue("designation_count");
    }
}
